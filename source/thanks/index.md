---
title: piscesys-thanks
date: 2023-01-02 20:53:32
---

### CutefishOS

- The official website was down, and its update was stopped.
- Thanks to its contribution to opensource community.
- But its baeuty and elegancy makes me unable to stop.
- We use the suffix `.cf` for this website with the aim of remembreing it.
- CutefishOS Team

---

### Dear developers

- They comes from the whole nation and even all over the world
- They hold their original aspiration which never die
- They don't ask for the reward, and they are devoted to opensource and Linux
- Piscesys Association
- Hope each developer can be happy everyday

---

### Hexo&Next

- They are the base of this website
- Using the friendly building method (from Markdown)
- Hope they can be more and more popular
- Hexo Team&Next Team

---

### Freenom

- They provides the domain of this website
- It's free and thresholdless
- Though it may not be used for a long time
- But it enabled that the website can be visit
- Hope it can be the familiar one to everybody
- Freenom

---

### Cloudflare Pages

- It hosted this website
- It's free and boundless
- High speed for national visit (China)
- Cloudflare Pages

---

### Developers

|         |Introduction|
|---------|:--:|
|<img src="https://gitlab.com/uploads/-/system/user/avatar/13366105/avatar.png" height=80 width=80 alt="wrefiyu" />|[wrefiyu lanka](https://gitlab.com/wrefiyu)<br />**project sponsor, website constructor, forum administrator**|
|<img src="https://gitlab.com/uploads/-/system/user/avatar/13367310/avatar.png" height=80 width=80 alt="wrefiyu" />|[Zhuang](https://gitlab.com/zhuangzhuang20080802)<br />**website constructor, forum creator and administrator**|
|<img src="https://gitlab.com/uploads/-/system/user/avatar/9567858/avatar.png" height=80 width=80 alt="wrefiyu" />|[Gavin Zhao](https://gitlab.com/GZGavinZhao)|
|<img src="https://gitlab.com/uploads/-/system/user/avatar/11697910/avatar.png" height=80 width=80 alt="wrefiyu" />|[月読時](https://gitlab.com/TsukuyomiToki)|
|<img src="https://gitlab.com/uploads/-/system/user/avatar/13339134/avatar.png" height=80 width=80 alt="wrefiyu" />|[Jack77793](https://gitlab.com/Jack77793)<br />**English website maintainer, forum moderator**|
