---
title: To-do list
date: 2023-01-04 15:21:27
---

1. Recruitment
2. Prepare for the new forum
3. Recover the repository of the project
4. Release the development version which based on Arch Linux on the basis of the Debian based mainline version
5. Solve the build problem
6. Broaden the propaganda channel
