---
title: hello
date: 2023-01-02 20:54:02
---

The website is built in a hurry today, here is the brief introduction of how to maintain the website.

Main page is direct to: `~/source/index.md`

Feishu is direct to: `~/source/feishu/index.md`

Thanks is direct to: `~/source/thanks/index.md`

you can clone it to localhost, modify and push up. Cloudflare Pages will build it automatically.

News is direct to the file in `~/source/_posts/`

You need to

```shell
npm install hexo-cil
```

first

And run

```shell
npx hexo new "title"
```

in the root directory

to create the new `title.md`

Notice：the file you create will contain a few lines of marks. What you need to do is just delete the line of 'tags'
